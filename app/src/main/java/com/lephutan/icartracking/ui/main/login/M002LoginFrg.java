package com.lephutan.icartracking.ui.main.login;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.databinding.FrgM002LoginBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;
import com.lephutan.icartracking.ui.dialog.ProgressDialog;

public class M002LoginFrg extends BaseBindingFragment<FrgM002LoginBinding, M002LoginVM> {
    private ProgressDialog dialog;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.tvButtonLogin.setOnClickListener(this);
        mBinding.tvCreatePass.setOnClickListener(this);
        mBinding.tvForgotPass.setOnClickListener(this);

        observeData();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_button_login:
                checkAccount();
                showDialogLoading();
                break;
            case R.id.tv_create_pass:
                goToRegister();
                break;
            case R.id.tv_forgot_pass:
                goToForgotPass();
                break;
        }
    }

    private void showDialogLoading() {
        dialog = new ProgressDialog();
        dialog.show(requireActivity().getSupportFragmentManager(), null);
    }

    private void goToForgotPass() {
        showFrg(M002LoginFrgDirections.actionM002LoginFrgToM005ForgotPassFrg());
    }

    private void goToRegister() {
        showFrg(M002LoginFrgDirections.actionM002LoginFrgToM001RegisterFrg());
    }

    private void checkAccount() {
        String phone = mBinding.edtPhone.getText().toString();
        String password = mBinding.edtPass.getText().toString();

        int result = mViewModel.validate(phone, password);

        switch (result) {
            case Constant.PHONE_OR_PASSWORD_IS_EMPTY:
                notify(R.string.txt_phone_number_or_password_is_empty);
                break;
            case Constant.INVALID_PHONE_NUMBER:
                notify(R.string.txt_invalid_phone_number);
                break;
            case Constant.INVALID_PASSWORD:
                notify(R.string.txt_invalid_password);
                break;
            case Constant.LOGIN_SUCCESS:
                mViewModel.loginAccount(phone, password);
                App.getInstance().getStorage().setPass(password);
                break;
        }
    }

    private void observeData() {
        notifyException();

        mViewModel.accLogin.observe(getViewLifecycleOwner(), new Observer<AccountLogin>() {
            @Override
            public void onChanged(AccountLogin accountLogin) {
                if (accountLogin.getMessage().equals("Đăng nhập thành công!")) {
                    dialog.dismiss();

                    App.getInstance().getStorage().setPhoneNumber(accountLogin.getData().getPhone());
                    App.getInstance().getStorage().setToken(accountLogin.getData().getToken());

                    showFrg(M002LoginFrgDirections.actionM002LoginFrgToM003MenuMainFrg());
                }
            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), integer -> {
            M002LoginFrg.this.notify(integer);
            dialog.dismiss();
        });
    }

    @Override
    protected Class<M002LoginVM> getViewModel() {
        return M002LoginVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m002_login;
    }
}
