package com.lephutan.icartracking.ui.main.change_password;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.remote.ICarApi;
import com.lephutan.icartracking.data.repository.ICarRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@HiltViewModel
public class M009ChangePasswordVM extends BaseViewModel {
    private final ICarApi iCarApi;
    MutableLiveData<Integer> smsC = new MutableLiveData<>();
    MutableLiveData<Integer> smsF = new MutableLiveData<>();

    @Inject
    public M009ChangePasswordVM(ICarApi iCarApi) {
        this.iCarApi = iCarApi;
    }

    public void changePass(String token, String oldPass, String newPass){
        iCarApi.changePassword("Bearer " + token, oldPass, newPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onComplete() {
                        smsC.postValue(R.string.doi_mat_khau_thanh_cong);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        smsF.postValue(R.string.sai_mat_khau_cu);
                    }
                });
    }

    public int validate(String oldPass, String newPass, String enterNewPass) {
        if (oldPass.isEmpty() || newPass.isEmpty() || enterNewPass.isEmpty()) {
            return Constant.PHONE_OR_PASSWORD_IS_EMPTY;
        }  else if (!checkPassword(oldPass, newPass, enterNewPass)) {
            return Constant.INVALID_PASSWORD;
        } else if (!duplicateCheck(newPass, enterNewPass)) {
            return Constant.PASSWORD_INCORRECT;
        }

        return Constant.LOGIN_SUCCESS;
    }

    private boolean duplicateCheck(String newPass, String enterNewPass) {
        return newPass.equals(enterNewPass);
    }

    private boolean checkPassword(String oldPass, String newPass, String enterNewPass) {
        return oldPass.length() >= 6 && newPass.length() >= 6 && enterNewPass.length() >= 6;
    }
}
