package com.lephutan.icartracking.ui.main;

import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class MainViewModel extends BaseViewModel {
    @Inject
    public MainViewModel() {

    }
}
