package com.lephutan.icartracking.ui.main.change_password;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.CommonUtils;
import com.lephutan.icartracking.databinding.FrgM009ChangePasswordBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

public class M009ChangePasswordFrg extends BaseBindingFragment<FrgM009ChangePasswordBinding, M009ChangePasswordVM> {
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbarChangePass.tvTitle.setText(R.string.txt_thong_tin_cap_nhat);
        mBinding.includeActionbarChangePass.ivBack.setOnClickListener(this);
        mBinding.tvButtonUpdate.setOnClickListener(this);

        observeData();
        initInfo();
        setupPhoneNumber();
    }

    private void initInfo() {
        String username = CommonUtils.getInstance().getPref(Constant.KEY_USER_NAME);

        if (username == null) {
            return;
        }

        mBinding.tvUser.setText(username);
    }

    private void setupPhoneNumber() {
        App.getInstance().getStorage().getPhoneNumber().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                mBinding.tvPhone.setText(s);
            }
        });
    }

    private void observeData() {
        mViewModel.smsC.observe(getViewLifecycleOwner(), M009ChangePasswordFrg.this::notify);
        mViewModel.smsF.observe(getViewLifecycleOwner(), M009ChangePasswordFrg.this::notify);
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.tv_button_update) {
            validatePass();
        } else if (v.getId() == R.id.iv_back) {
            onBacked();
        }
    }

    private void validatePass() {
        String oldPass = mBinding.edtOldPass.getText().toString();
        String newPass = mBinding.edtNewPass.getText().toString();
        String enterNewPass = mBinding.edtEnterNewPass.getText().toString();

        String token = App.getInstance().getStorage().getToken().getValue();

        if (token == null) {
            return;
        }

        int result = mViewModel.validate(oldPass, newPass, enterNewPass);
        switch (result) {
            case Constant.PHONE_OR_PASSWORD_IS_EMPTY:
                notify(R.string.txt_khong_de_trong_o_nhap);
                break;
            case Constant.INVALID_PASSWORD:
                notify(R.string.txt_invalid_password);
                break;
            case Constant.PASSWORD_INCORRECT:
                notify(R.string.txt_password_incorrect);
                break;
            case Constant.LOGIN_SUCCESS:
                mViewModel.changePass(token, oldPass, newPass);
                break;
        }
    }

    @Override
    protected Class<M009ChangePasswordVM> getViewModel() {
        return M009ChangePasswordVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m009_change_password;
    }
}
