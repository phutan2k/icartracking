package com.lephutan.icartracking.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lephutan.icartracking.databinding.ItemPhotoBinding;
import com.lephutan.icartracking.interfaces.OnItemCallBack;

import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder> {
    private final Context mContext;
    private OnItemCallBack onItemCallBack;
    private List<Integer> itemList;
    private int id = -1;

    public PhotoAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnItemCallBack(OnItemCallBack onItemCallBack) {
        this.onItemCallBack = onItemCallBack;
    }

    public void setItemList(List<Integer> itemList) {
        this.itemList = itemList;
    }

    public void setId(int pos) {
        id = pos;
    }

    @NonNull
    @Override
    public PhotoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPhotoBinding mBinding = ItemPhotoBinding.inflate
                (
                        LayoutInflater.from(parent.getContext()), parent, false
                );
        return new PhotoHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoHolder holder, int position) {
        holder.bind(itemList.get(position), mContext, position);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class PhotoHolder extends RecyclerView.ViewHolder {
        ItemPhotoBinding mBinding;

        public PhotoHolder(@NonNull ItemPhotoBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Integer photo, Context mContext, int position) {
            mBinding.changeWallpaperBtn.setBackground(ContextCompat.getDrawable(mContext, photo));

            mBinding.changeWallpaperBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemCallBack.onImageButtonClick(photo, position);

                    if (id != position) {
                        notifyItemChanged(id);
                        id = position;
                        notifyItemChanged(position);
                    }
                }
            });

            if (id == position) {
                mBinding.isSelected.setVisibility(View.VISIBLE);
            } else {
                mBinding.isSelected.setVisibility(View.GONE);
            }
        }
    }
}
