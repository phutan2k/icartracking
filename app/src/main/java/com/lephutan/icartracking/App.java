package com.lephutan.icartracking;

import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.BuildConfig;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.lephutan.icartracking.common.Storage;
import com.lephutan.icartracking.utils.LocaleUtils;
import com.lephutan.icartracking.utils.MyDebugTree;

import dagger.hilt.android.HiltAndroidApp;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import timber.log.Timber;

@HiltAndroidApp
public class App extends MultiDexApplication {
    private static App instance;
    private Storage storage;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        storage = new Storage();
        RxJavaPlugins.setErrorHandler(Timber::w);
        initLog();
    }

    public Storage getStorage() {
        return storage;
    }

    private void initLog() {
        if (BuildConfig.DEBUG) {
            Timber.e("lephutan onCreate: ");

            Timber.plant(new MyDebugTree());
        }
    }

    public static App getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.applyLocale(this);
    }
}
