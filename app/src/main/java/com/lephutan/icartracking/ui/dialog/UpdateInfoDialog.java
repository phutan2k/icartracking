package com.lephutan.icartracking.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.CommonUtils;
import com.lephutan.icartracking.databinding.ViewUpdateInfoBinding;
import com.lephutan.icartracking.ui.base.BaseBindingDialogFragment;

import java.util.Objects;

public class UpdateInfoDialog extends BaseBindingDialogFragment<ViewUpdateInfoBinding> {

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog()).setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

        getDialog().getWindow().setLayout
                (
                        (int) (getResources().getDisplayMetrics().widthPixels * 0.9),
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        App.getInstance().getStorage().getPhoneNumber().observe(
                getViewLifecycleOwner(), phone ->
                        mBinding.tvPhone.setText(phone)
        );

        mBinding.tvButtonUpdate.setOnClickListener(this);
        mBinding.ivBack.setOnClickListener(this);
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.tv_button_update) {
            checkName();
        } else if (v.getId() == R.id.iv_back) {
            dismiss();
        }
    }

    private void checkName() {
        String name = mBinding.edtUserName.getText().toString();

        if (name.equals("") || name.isEmpty()) {
            notify(R.string.txt_khong_de_trong_o_nhap);
        } else {
            String username = mBinding.edtUserName.getText().toString();
            CommonUtils.getInstance().savePref(Constant.KEY_USER_NAME, username);
            App.getInstance().getStorage().setName(username);
            dismiss();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_update_info;
    }
}
