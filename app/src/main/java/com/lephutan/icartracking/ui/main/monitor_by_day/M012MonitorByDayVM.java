package com.lephutan.icartracking.ui.main.monitor_by_day;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.repository.ICarLocalRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class M012MonitorByDayVM extends BaseViewModel {
    private final ICarLocalRepository iCarLocalRepository;
    MutableLiveData<List<DetailHistory>> detailHistoryList = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public M012MonitorByDayVM(ICarLocalRepository iCarLocalRepository) {
        this.iCarLocalRepository = iCarLocalRepository;
    }

    public void getDetailHistoryList(int id) {
        iCarLocalRepository.getDetailHistoryList(id).subscribe(new SingleObserver<List<DetailHistory>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull List<DetailHistory> detailHistories) {
                detailHistoryList.postValue(detailHistories);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.something_went_wrong);
            }
        });
    }
}
