package com.lephutan.icartracking.ui.main.monitor;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.SupportMapFragment;
import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.databinding.FrgM011MonitorBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;
import com.lephutan.icartracking.ui.google_map.MapManager;
import com.lephutan.icartracking.ui.google_map.MapMonitorManager;

public class M011MonitorFrg extends BaseBindingFragment<FrgM011MonitorBinding, M011MonitorVM> {
    private ICar myCar;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        myCar = App.getInstance().getStorage().getCar().getValue();
        if (myCar == null) {
            return;
        }
        mBinding.tvCarNumber.setText(myCar.carNumber);

        mBinding.ivBack.setOnClickListener(this);
        mBinding.ivLocation.setOnClickListener(this);
        mBinding.ivCarLocation.setOnClickListener(this);

        setupMap();
    }

    private void setupMap() {
        SupportMapFragment mapFrg = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_monitor_frg);
        assert mapFrg != null;
        mapFrg.getMapAsync(googleMap -> MapMonitorManager.getInstance().initMap(googleMap));
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void clickView(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBacked();
                break;
            case R.id.iv_location:
                MapMonitorManager.getInstance().updateMyLocation();
                break;
            case R.id.iv_car_location:
                MapMonitorManager.getInstance().showCar(myCar);
                break;
        }
    }

    @Override
    protected Class<M011MonitorVM> getViewModel() {
        return M011MonitorVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m011_monitor;
    }
}
