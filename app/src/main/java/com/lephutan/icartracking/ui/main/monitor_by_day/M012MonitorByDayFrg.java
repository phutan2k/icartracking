package com.lephutan.icartracking.ui.main.monitor_by_day;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.Observer;

import com.google.android.gms.maps.SupportMapFragment;
import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.databinding.FrgM012MonitorByDayBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;
import com.lephutan.icartracking.ui.google_map.MapMonitoringByDayManager;

import java.util.List;

public class M012MonitorByDayFrg extends BaseBindingFragment<FrgM012MonitorByDayBinding, M012MonitorByDayVM> {
    private History history;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        history = App.getInstance().getStorage().getHistory().getValue();

        if (history == null) {
            return;
        }
        mBinding.includeActionbarMonitor.tvTitle.setText(history.date);

        mBinding.includeActionbarMonitor.ivBack.setOnClickListener(this);
        mBinding.ivLocation.setOnClickListener(this);
        mBinding.ivCarLocation.setOnClickListener(this);

        setupMap();
        observeData();
    }

    private void observeData() {
        notifyException();

        mViewModel.detailHistoryList.observe(getViewLifecycleOwner(), new Observer<List<DetailHistory>>() {
            @Override
            public void onChanged(List<DetailHistory> historyList) {
                MapMonitoringByDayManager.getInstance().showCarList(historyList);
            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M012MonitorByDayFrg.this::notify);
    }

    private void setupMap() {
        SupportMapFragment mapFrg = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_monitor_by_day_frg);
        assert mapFrg != null;
        mapFrg.getMapAsync(googleMap -> MapMonitoringByDayManager.getInstance().initMap(googleMap));
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void clickView(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBacked();
                break;
            case R.id.iv_location:
                MapMonitoringByDayManager.getInstance().updateMyLocation();
                break;
            case R.id.iv_car_location:
                mViewModel.getDetailHistoryList(history.dateId);
                break;
        }
    }

    @Override
    protected Class<M012MonitorByDayVM> getViewModel() {
        return M012MonitorByDayVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m012_monitor_by_day;
    }
}
