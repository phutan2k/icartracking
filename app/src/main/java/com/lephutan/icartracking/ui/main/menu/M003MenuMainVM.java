package com.lephutan.icartracking.ui.main.menu;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.repository.ICarLocalRepository;
import com.lephutan.icartracking.data.repository.ICarRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class M003MenuMainVM extends BaseViewModel {
    private final ICarLocalRepository iCarLocalRepository;
    private final ICarRepository iCarRepository;

    MutableLiveData<List<ICar>> carList = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();
    MutableLiveData<AccountLogin> accLogin = new MutableLiveData<>();

    @Inject
    public M003MenuMainVM(ICarLocalRepository iCarLocalRepository, ICarRepository iCarRepository) {
        this.iCarLocalRepository = iCarLocalRepository;
        this.iCarRepository = iCarRepository;
    }

    public void getCarList() {
        iCarLocalRepository.getCarList().subscribe(new SingleObserver<List<ICar>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull List<ICar> iCars) {
                carList.postValue(iCars);
            }

            @SuppressLint("LogNotTimber")
            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.something_went_wrong);
                Log.i("LePhuTan", e.getMessage());
            }
        });
    }

    public void loginAccount(String phone, String password) {
        iCarRepository.loginAccount(phone, password)
                .subscribe(new SingleObserver<AccountLogin>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull AccountLogin accountLogin) {
                        accLogin.postValue(accountLogin);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.something_went_wrong);
                    }
                });
    }
}
