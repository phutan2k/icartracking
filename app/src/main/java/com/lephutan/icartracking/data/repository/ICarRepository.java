package com.lephutan.icartracking.data.repository;

import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.model.authen.UserData;
import com.lephutan.icartracking.data.remote.ICarApi;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ICarRepository {
    private final ICarApi iCarApi;

    @Inject
    public ICarRepository(ICarApi iCarApi) {
        this.iCarApi = iCarApi;
    }

    /*Login Authen*/
    public Single<AccountLogin> loginAccount(String phone, String password) {
        return iCarApi.loginAccount(phone, password).subscribeOn(Schedulers.io());
    }

    /*Register Authen*/
    public Single<AccountLogin> registerAccount(String phone, String password) {
        return iCarApi.registerAccount(phone, password).subscribeOn(Schedulers.io());
    }

    /*Check Phone*/
    public Single<AccountLogin> checkPhone(String phone) {
        return iCarApi.checkPhone(phone).subscribeOn(Schedulers.io());
    }

    /*Forgot Password*/
    public Single<AccountLogin> forgotPassword(String phone, String newPass) {
        return iCarApi.forgotPassword(phone, newPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
