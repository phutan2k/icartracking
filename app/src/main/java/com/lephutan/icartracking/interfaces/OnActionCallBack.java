package com.lephutan.icartracking.interfaces;

public interface OnActionCallBack {
    void callBack(Object data, String key);
}
