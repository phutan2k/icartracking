package com.lephutan.icartracking.data.repository;

import com.lephutan.icartracking.data.local.db.ICarDao;
import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.data.local.db.entities.ICar;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ICarLocalRepository {
    private final ICarDao iCarDao;

    @Inject
    public ICarLocalRepository(ICarDao iCarDao) {
        this.iCarDao = iCarDao;
    }

    public Single<List<ICar>> getCarList() {
        return iCarDao.getCarList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<History>> getHistoryList(int id) {
        return iCarDao.getHistoryList(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<DetailHistory>> getDetailHistoryList(int id){
        return iCarDao.getDetailHistoryList(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
