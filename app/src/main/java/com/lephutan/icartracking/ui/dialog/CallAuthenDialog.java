package com.lephutan.icartracking.ui.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.databinding.ViewCallAuthenBinding;
import com.lephutan.icartracking.ui.base.BaseBindingDialogFragment;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class CallAuthenDialog extends BaseBindingDialogFragment<ViewCallAuthenBinding> {
    private static final String TAG = CallAuthenDialog.class.getName();
    private final String phoneNumber;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    public CallAuthenDialog(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog()).setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

        getDialog().getWindow().setLayout
                (
                        (int) (getResources().getDisplayMetrics().widthPixels * 0.9),
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.ivBack.setOnClickListener(this);
        mBinding.tvButtonConfirm.setOnClickListener(this);
        mBinding.tvPhone.setText(phoneNumber);

        initViews();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void clickView(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                dismiss();
                break;
            case R.id.tv_button_confirm:
                validatePhone(mBinding.edtOtp.getText().toString());
                break;
        }
    }

    private void initViews() {
        mAuth = FirebaseAuth.getInstance();
        initCallBackAuthen();

        getCode(phoneNumber);
    }

    @SuppressLint("LogNotTimber")
    private void initCallBackAuthen() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                // show a message and update UI
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);

                mVerificationId = verificationId;
                mResendToken = token;
            }
        };
    }

    private void getCode(String phone) {
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(phone)                      // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS)  // Timeout and unit
                        .setActivity(requireActivity())            // Activity (for callback binding)
                        .setCallbacks(mCallbacks)                 // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private void validatePhone(String otp) {
        if (otp.isEmpty()) {
            notify(R.string.txt_khong_de_trong_o_nhap);
        } else {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            signInWithPhoneAuthCredential(credential);
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            CallAuthenDialog.this.notify(R.string.xac_thuc_thanh_cong);
                            dismiss();
                        } else {
                            CallAuthenDialog.this.notify(R.string.xac_thuc_loi);
                        }
                    }
                });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_call_authen;
    }
}
