package com.lephutan.icartracking.ui.main.change_username;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.CommonUtils;
import com.lephutan.icartracking.databinding.FrgM013ChangeUsernameBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

public class M013ChangeUsernameFrg extends BaseBindingFragment<FrgM013ChangeUsernameBinding, M013ChangeUsernameVM> {
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbarChangePass.tvTitle.setText(R.string.txt_thong_tin_cap_nhat);
        mBinding.includeActionbarChangePass.ivBack.setOnClickListener(this);
        mBinding.tvButtonUpdate.setOnClickListener(this);

        setupPhoneNumber();
        observeData();
    }

    private void observeData() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M013ChangeUsernameFrg.this::notify);
    }

    private void setupPhoneNumber() {
        App.getInstance().getStorage().getPhoneNumber().observe(
                getViewLifecycleOwner(), phone ->
                        mBinding.tvPhone.setText(phone)
        );
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.iv_back) {
            onBacked();
        } else if (v.getId() == R.id.tv_button_update) {
            validateUsername();
        }
    }

    private void validateUsername() {
        String username = mBinding.edtUserName.getText().toString();

        String token = App.getInstance().getStorage().getToken().getValue();

        if (token == null) {
            return;
        }

        int result = mViewModel.checkUsername(username);
        if (result == Constant.USER_NAME_IS_EMPTY) {
            notify(R.string.txt_khong_de_trong_o_nhap);
        } else if (result == Constant.LOGIN_SUCCESS) {
            mViewModel.updateUsername(token, username);
        }
    }

    @Override
    protected Class<M013ChangeUsernameVM> getViewModel() {
        return M013ChangeUsernameVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m013_change_username;
    }
}
