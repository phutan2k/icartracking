package com.lephutan.icartracking.ui.main.info_user;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.repository.ICarRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class M007InfoUserVM extends BaseViewModel {
    private final ICarRepository iCarRepository;
    MutableLiveData<AccountLogin> accLogin = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public M007InfoUserVM(ICarRepository iCarRepository) {
        this.iCarRepository = iCarRepository;
    }

    public void loginAccount(String phone, String password) {
        iCarRepository.loginAccount(phone, password)
                .subscribe(new SingleObserver<AccountLogin>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull AccountLogin accountLogin) {
                        accLogin.postValue(accountLogin);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.something_went_wrong);
                    }
                });
    }
}
