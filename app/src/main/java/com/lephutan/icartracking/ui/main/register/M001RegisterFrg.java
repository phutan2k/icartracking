package com.lephutan.icartracking.ui.main.register;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.databinding.FrgM001RegisterBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;
import com.lephutan.icartracking.ui.dialog.CallAuthenDialog;

public class M001RegisterFrg extends BaseBindingFragment<FrgM001RegisterBinding, M001RegisterVM> {
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbarRegister.tvTitle.setText(R.string.txt_dang_ky);

        mBinding.tvButtonRegister.setOnClickListener(this);
        mBinding.includeActionbarRegister.ivBack.setOnClickListener(this);

        observeData();
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.tv_button_register) {
            checkAccount();
        } else if (v.getId() == R.id.iv_back) {
            onBacked();
        }
    }

    private void checkAccount() {
        String phone = mBinding.edtPhone.getText().toString();
        String password = mBinding.edtPass.getText().toString();
        String reEnterPassword = mBinding.edtReEnterPass.getText().toString();

        String newPhoneNumber = mViewModel.changeNumberFormat(phone);

        int result = mViewModel.validate(phone, password, reEnterPassword);

        switch (result) {
            case Constant.PHONE_OR_PASSWORD_IS_EMPTY:
                notify(R.string.txt_phone_number_or_password_is_empty);
                break;
            case Constant.INVALID_PHONE_NUMBER:
                notify(R.string.txt_invalid_phone_number);
                break;
            case Constant.INVALID_PASSWORD:
                notify(R.string.txt_invalid_password);
                break;
            case Constant.PASSWORD_INCORRECT:
                notify(R.string.txt_password_incorrect);
                break;
            case Constant.LOGIN_SUCCESS:
                // Call dịch vụ fire base để lấy số điện thoại
                showDialog(newPhoneNumber);

                mViewModel.registerAccount(phone, password);
                break;
        }
    }

    private void observeData() {
        notifyException();

        mViewModel.accRegister.observe(getViewLifecycleOwner(), new Observer<AccountLogin>() {
            @Override
            public void onChanged(AccountLogin accountLogin) {
                if (accountLogin.getMessage().equals("Đăng ký tài khoản thành công!")) {
                    onBacked();
                }
            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                M001RegisterFrg.this.notify(R.string.account_registration_failed);
            }
        });
    }

    private void showDialog(String phone) {
        CallAuthenDialog dialog = new CallAuthenDialog(phone);
        dialog.show(requireActivity().getSupportFragmentManager(), null);
    }

    @Override
    protected Class<M001RegisterVM> getViewModel() {
        return M001RegisterVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m001_register;
    }
}
