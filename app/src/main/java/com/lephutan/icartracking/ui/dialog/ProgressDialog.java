package com.lephutan.icartracking.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.databinding.ViewProgressBinding;
import com.lephutan.icartracking.ui.base.BaseBindingDialogFragment;

import java.util.Objects;

public class ProgressDialog extends BaseBindingDialogFragment<ViewProgressBinding> {
    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog()).setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

        getDialog().getWindow().setLayout
                (
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        Animation animLoading = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
        mBinding.ivLoading.setAnimation(animLoading);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_progress;
    }
}
