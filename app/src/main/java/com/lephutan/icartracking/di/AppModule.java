package com.lephutan.icartracking.di;

import android.app.Application;

import androidx.room.Room;

import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.ICarDB;
import com.lephutan.icartracking.data.local.db.ICarDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class AppModule {

    @Provides
    @Singleton
    public ICarDB provideRoomDb(Application context) {
        return Room.databaseBuilder(context, ICarDB.class, Constant.NAME_DB)
                .createFromAsset("icar.db").build();
    }

    @Provides
    @Singleton
    public ICarDao provideICarThreadDao(ICarDB db) {
        return db.getICarDao();
    }
}
