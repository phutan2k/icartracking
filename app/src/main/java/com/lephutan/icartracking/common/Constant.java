package com.lephutan.icartracking.common;

public class Constant {
    public static final String BASE_URL = "https://icar-api.abank.com.vn/api/v1/";

    public static final int CONNECT_S = 30;
    public static final int WRITE_S = 30;
    public static final int READ_S = 30;
    public static final String PREF_SETTING_LANGUAGE = "pref_setting_language";
    public static final String LANGUAGE_EN = "en";

    public static final int PHONE_OR_PASSWORD_IS_EMPTY = 50;
    public static final int LOGIN_SUCCESS = 200;
    public static final int INVALID_PHONE_NUMBER = 51;
    public static final int INVALID_PASSWORD = 52;
    public static final int PASSWORD_INCORRECT = 53;
    public static final int USER_NAME_IS_EMPTY = 54;

    public static final int LEVEL_OPEN = 1;
    public static final int LEVEL_CLOSE = 0;

    public static final String KEY_ID = "KEY_ID";
    public static final String KEY_IMG = "KEY_IMG";
    public static final String KEY_USER_NAME = "KEY_USER_NAME";
    public static final String STATE_LEVEL_MENU = "STATE_LEVEL_MENU";

    public static final int VERSION_DB = 1;
    public static final String NAME_DB = "icar.db";

    public static final String KEY_SHOW_DETAIL_CAR = "KEY_SHOW_DETAIL_CAR";
    public static final String KEY_SHOW_M011_MONITOR_FRG = "KEY_SHOW_M011_MONITOR_FRG";
    public static final String KEY_SHOW_M004_TIME_LINE_FRG = "KEY_SHOW_M004_TIME_LINE_FRG";
    public static final String KEY_SHOW_M012_MONITOR_BY_DAY_FRG = "KEY_SHOW_M012_MONITOR_BY_DAY_FRG";

    public static final String KEY_GET_DETAIL_HISTORY = "KEY_GET_DETAIL_HISTORY";
}
