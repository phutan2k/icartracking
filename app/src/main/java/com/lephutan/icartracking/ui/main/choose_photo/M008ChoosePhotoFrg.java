package com.lephutan.icartracking.ui.main.choose_photo;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.CommonUtils;
import com.lephutan.icartracking.databinding.FrgM008ChoosePhotoBinding;
import com.lephutan.icartracking.interfaces.OnItemCallBack;
import com.lephutan.icartracking.ui.adapter.PhotoAdapter;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

public class M008ChoosePhotoFrg extends BaseBindingFragment<FrgM008ChoosePhotoBinding, M008ChoosePhotoVM> {
    private PhotoAdapter mAdapter;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbarImg.tvTitle.setText(R.string.txt_gallery);

        mBinding.includeActionbarImg.ivBack.setOnClickListener(this);

        initData();
        stateId();
    }

    private void stateId() {
        Integer pos = CommonUtils.getInstance().getInt(Constant.KEY_ID);

        if (pos == null)
            return;
        if (pos != -1) {
            mAdapter.setId(pos);
        }
    }

    private void initData() {
        GridLayoutManager layoutManager = new GridLayoutManager(requireContext(), 2);
        mBinding.rvListPhoto.setLayoutManager(layoutManager);

        if(mAdapter == null){
            mAdapter = new PhotoAdapter(requireContext());
            mBinding.rvListPhoto.setAdapter(mAdapter);

            mAdapter.setItemList(mViewModel.getPhotoList());
        }

        mAdapter.setOnItemCallBack(new OnItemCallBack() {
            @Override
            public void onImageButtonClick(int img, int pos) {
                App.getInstance().getStorage().setImg(img);
                App.getInstance().getStorage().setPos(pos);

                CommonUtils.getInstance().saveInt(Constant.KEY_ID, pos);
                CommonUtils.getInstance().saveInt(Constant.KEY_IMG, img);
            }
        });
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.iv_back) {
            onBacked();
        }
    }

    @Override
    protected Class<M008ChoosePhotoVM> getViewModel() {
        return M008ChoosePhotoVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m008_choose_photo;
    }
}
