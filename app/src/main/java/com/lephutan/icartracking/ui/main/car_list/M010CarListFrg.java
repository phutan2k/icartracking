package com.lephutan.icartracking.ui.main.car_list;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.databinding.FrgM010CarListBinding;
import com.lephutan.icartracking.ui.adapter.CarAdapter;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

import java.util.List;

public class M010CarListFrg extends BaseBindingFragment<FrgM010CarListBinding, M010CarListVM> {
    private CarAdapter adapter;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbarCar.tvTitle.setText(R.string.txt_list_cars);
        mBinding.includeActionbarCar.ivBack.setOnClickListener(this);

        initData();
        observeData();
    }

    private void observeData() {
        notifyException();

        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        mBinding.rvListCars.setLayoutManager(layoutManager);

        mViewModel.carList.observe(getViewLifecycleOwner(), new Observer<List<ICar>>() {
            @Override
            public void onChanged(List<ICar> iCars) {
                String phone = App.getInstance().getStorage().getPhoneNumber().getValue();

                if (phone == null) {
                    return;
                }

                if (phone.equals(iCars.get(0).phoneManager)) {
                    if (adapter == null) {
                        adapter = new CarAdapter();
                        adapter.setCarList(iCars);
                        mBinding.rvListCars.setAdapter(adapter);

                        adapter.setCallBack((data, key) -> {
                            ICar iCar = (ICar) data;
                            App.getInstance().getStorage().setCar(iCar);

                            if (key.equals(Constant.KEY_SHOW_M011_MONITOR_FRG)) {
                                showFrg(M010CarListFrgDirections.actionM010CarListFrgToM011MonitorFrg());
                            } else if (key.equals(Constant.KEY_SHOW_M004_TIME_LINE_FRG)) {
                                showFrg(M010CarListFrgDirections.actionM010CarListFrgToM004TimeLineFrg());
                            }
                        });
                    }
                } else {
                    M010CarListFrg.this.notify(R.string.txt_khong_co_xe_trong_ds_quan_ly);
                }
            }
        });
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.iv_back) {
            onBacked();
        }
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M010CarListFrg.this::notify);
    }

    private void initData() {
        mViewModel.getCarList();
    }

    @Override
    protected Class<M010CarListVM> getViewModel() {
        return M010CarListVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m010_car_list;
    }
}
