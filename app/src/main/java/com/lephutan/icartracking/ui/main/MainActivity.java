package com.lephutan.icartracking.ui.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.RequiresApi;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.databinding.ActivityMainBinding;
import com.lephutan.icartracking.ui.base.BaseBindingActivity;


public class MainActivity extends BaseBindingActivity<ActivityMainBinding, MainViewModel> {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void setupData() {
        checkLocationPermission();
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        Handler mHandler = new Handler();
        mHandler.postDelayed(() -> changeStartDestination(R.id.m002LoginFrg), 2000);
    }

    @Override
    protected Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkLocationPermission() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED)) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            }, 101);
        }
    }
}