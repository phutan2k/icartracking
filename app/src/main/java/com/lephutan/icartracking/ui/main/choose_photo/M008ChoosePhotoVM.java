package com.lephutan.icartracking.ui.main.choose_photo;

import com.lephutan.icartracking.data.repository.PhotoRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import java.util.List;

public class M008ChoosePhotoVM extends BaseViewModel {
    private final PhotoRepository photoRepository;

    public M008ChoosePhotoVM() {
        photoRepository = new PhotoRepository();
    }

    public List<Integer> getPhotoList() {
        return photoRepository.getImageList();
    }
}
