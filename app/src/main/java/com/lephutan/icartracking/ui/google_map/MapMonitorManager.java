package com.lephutan.icartracking.ui.google_map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.ICar;

import java.util.ArrayList;
import java.util.List;

public class MapMonitorManager {
    public static final String TAG = MapMonitorManager.class.getName();
    public static MapMonitorManager instance;
    private GoogleMap mMap;

    private Location myLocation;
    private Marker mMarker;
    private Polyline polyline;
    private LatLng pos;

    public MapMonitorManager() {
        // for singleton
    }

    public static MapMonitorManager getInstance() {
        if (instance == null) {
            instance = new MapMonitorManager();
        }
        return instance;
    }

    @SuppressLint("VisibleForTests")
    public void initMap(GoogleMap map) {
        mMap = map;
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(App.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(App.getInstance(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            Toast.makeText
                    (App.getInstance(), R.string.ban_chua_cap_quyen_vi_tri, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        LocationRequest request = new LocationRequest();
        request.setInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        FusedLocationProviderClient mClient = new FusedLocationProviderClient(App.getInstance());
        mClient.requestLocationUpdates(request, new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                // khi thay đổi vị trí nó sẽ cập nhật vào đây
                updateMyLocation(locationResult);
            }
        }, Looper.getMainLooper());
    }

    @SuppressLint("LogNotTimber")
    private void updateMyLocation(LocationResult rs) {
        Location location = rs.getLastLocation();

        if (myLocation == null) {
            myLocation = location;
            pos = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));

            // hiển thị marker
            MarkerOptions op = new MarkerOptions();

            op.title("My location");
            op.position(pos);
            op.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_flag));
            mMarker = mMap.addMarker(op);
        } else {
            myLocation = location;
        }
    }

    public void updateMyLocation() {
        if (mMap == null || myLocation == null) return;
        LatLng pos = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        mMarker.setPosition(pos);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));
    }

    @SuppressLint("LogNotTimber")
    public void showCar(ICar car) {
        double lat = Double.parseDouble(car.lastLat);
        double lng = Double.parseDouble(car.lastLng);
        LatLng posCar = new LatLng(lat, lng);

        MarkerOptions op = new MarkerOptions();
        op.title(car.carNumber);
        op.position(posCar);
        op.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_location));

        PolylineOptions polyOp = new PolylineOptions();
        polyOp.color(Color.RED);
        polyOp.width(10F);
        polyOp.zIndex(1F);
        polyOp.add(pos, posCar);

        polyline = mMap.addPolyline(polyOp);

        mMap.addMarker(op);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 12));
    }
}
