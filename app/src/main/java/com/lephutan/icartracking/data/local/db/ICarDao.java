package com.lephutan.icartracking.data.local.db;

import androidx.room.Dao;
import androidx.room.Query;

import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.data.local.db.entities.ICar;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

@Dao
public interface ICarDao {
    @Query("SELECT * FROM icar")
    Single<List<ICar>> getCarList();

    @Query("SELECT * FROM history WHERE car_id = :id")
    Single<List<History>> getHistoryList(int id);

    @Query("SELECT * FROM detail_history WHERE date_id = :id")
    Single<List<DetailHistory>> getDetailHistoryList(int id);
}
