package com.lephutan.icartracking.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.databinding.ItemDetailHistoryBinding;

import java.util.List;

public class DetailTimeLineAdapter extends RecyclerView.Adapter<DetailTimeLineAdapter.DetailTimeLineHolder> {
    private List<DetailHistory> detailHistoryList;



    public void setDetailHistoryList(List<DetailHistory> detailHistoryList) {
        this.detailHistoryList = detailHistoryList;
    }

    @NonNull
    @Override
    public DetailTimeLineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDetailHistoryBinding mBinding = ItemDetailHistoryBinding.inflate
                (
                        LayoutInflater.from(parent.getContext()), parent, false
                );
        return new DetailTimeLineHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailTimeLineHolder holder, int position) {
        holder.bind(detailHistoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return detailHistoryList.size();
    }

    public static class DetailTimeLineHolder extends RecyclerView.ViewHolder {
        ItemDetailHistoryBinding mBinding;

        public DetailTimeLineHolder(@NonNull ItemDetailHistoryBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(DetailHistory detailHistory) {
            mBinding.tvTime.setText(detailHistory.timeHistory);
            mBinding.tvStatus.setText(detailHistory.lastStatus);
            mBinding.tvSpeed.setText(detailHistory.lastSpeed);
            mBinding.tvAddress.setText(detailHistory.lastAddress);
        }
    }
}
