package com.lephutan.icartracking.ui.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.databinding.ViewDetailCarBinding;
import com.lephutan.icartracking.interfaces.OnActionCallBack;
import com.lephutan.icartracking.ui.base.BaseBindingDialogFragment;

import java.util.Objects;

public class DetailCarDialog extends BaseBindingDialogFragment<ViewDetailCarBinding> {
    private final ICar car;
    private OnActionCallBack callBack;

    public DetailCarDialog(ICar car) {
        this.car = car;
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog()).setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);

        getDialog().getWindow().setLayout
                (
                        (int) (getResources().getDisplayMetrics().widthPixels * 0.9),
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.ivBack.setOnClickListener(this);
        mBinding.tvButtonMonitor.setOnClickListener(this);
        mBinding.tvButtonHistory.setOnClickListener(this);

        initData(car);
    }

    private void initData(ICar car) {
        mBinding.tvCarNumber.setText(car.carNumber);
        mBinding.tvCarName.setText(car.carName);
        mBinding.tvLastStatus.setText(car.lastStatus);
        mBinding.tvLastSpeed.setText(car.lastSpeed);
        mBinding.tvLastAddress.setText(car.lastAddress);

        mBinding.tvButtonMonitor.setOnClickListener(v ->
                callBack.callBack(car, Constant.KEY_SHOW_M011_MONITOR_FRG));

        mBinding.tvButtonHistory.setOnClickListener(v ->
                callBack.callBack(car, Constant.KEY_SHOW_M004_TIME_LINE_FRG));
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.iv_back) {
            dismiss();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_detail_car;
    }
}
