package com.lephutan.icartracking.ui.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.lephutan.icartracking.interfaces.OnShowFrgCallBack;

public abstract class BaseBindingDialogFragment<T extends ViewDataBinding>
        extends BaseDialogFragment implements View.OnClickListener {
    protected T mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onCreatedView(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        clickView(v);
    }

    protected void clickView(View v) {
        // do something
    }

    protected final void notify(String sms) {
        Toast.makeText(getContext(), sms, Toast.LENGTH_SHORT).show();
    }

    protected final void notify(int sms) {
        Toast.makeText(getContext(), sms, Toast.LENGTH_SHORT).show();
    }

    protected abstract void onCreatedView(View view, Bundle savedInstanceState);

    protected abstract int getLayoutId();

}
