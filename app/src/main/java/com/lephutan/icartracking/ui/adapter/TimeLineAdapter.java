package com.lephutan.icartracking.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.databinding.ItemHistoryBinding;
import com.lephutan.icartracking.interfaces.OnActionCallBack;

import java.util.List;

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineHolder> {
    private final Context context;
    private List<History> historyList;
    private OnActionCallBack callBack;
    private List<DetailHistory> detailHistoryList;

    @SuppressLint("NotifyDataSetChanged")
    public TimeLineAdapter(Context context) {
        notifyDataSetChanged();
        this.context = context;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setDetailHistoryList(List<DetailHistory> detailHistoryList) {
        this.detailHistoryList = detailHistoryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TimeLineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHistoryBinding mBinding = ItemHistoryBinding.inflate
                (
                        LayoutInflater.from(parent.getContext()), parent, false
                );
        return new TimeLineHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeLineHolder holder, int position) {
        holder.bind(historyList.get(position));
        setupAdapterDetailHistory(holder);
    }

    private void setupAdapterDetailHistory(TimeLineHolder holder) {
        if (detailHistoryList == null) return;

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        holder.mBinding.rvDetailHistory.setLayoutManager(layoutManager);

        DetailTimeLineAdapter adapter = new DetailTimeLineAdapter();
        adapter.setDetailHistoryList(detailHistoryList);
        holder.mBinding.rvDetailHistory.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    private void showDetailHistory(History history) {
        callBack.callBack(history, Constant.KEY_GET_DETAIL_HISTORY);
    }

    public class TimeLineHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemHistoryBinding mBinding;
        History myHistory;

        public TimeLineHolder(@NonNull ItemHistoryBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(History history) {
            myHistory = history;

            mBinding.tvDate.setText(history.date);
            mBinding.ivReload.setOnClickListener(this);
            mBinding.ivTimeline.setOnClickListener(this);
            mBinding.ivShow.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.iv_show) {
                if (mBinding.ivShow.getDrawable().getLevel() == 0) {
                    mBinding.ivShow.getDrawable().setLevel(1);

                    mBinding.rvDetailHistory.setVisibility(View.VISIBLE);
                    mBinding.ivReload.setVisibility(View.VISIBLE);
                    mBinding.ivTimeline.setVisibility(View.VISIBLE);

                    showDetailHistory(myHistory);
                } else {
                    mBinding.ivShow.getDrawable().setLevel(0);

                    mBinding.rvDetailHistory.setVisibility(View.GONE);
                    mBinding.ivReload.setVisibility(View.GONE);
                    mBinding.ivTimeline.setVisibility(View.GONE);
                }
            } else if (v.getId() == R.id.iv_timeline) {
                callBack.callBack(myHistory, Constant.KEY_SHOW_M012_MONITOR_BY_DAY_FRG);
            } else if (v.getId() == R.id.iv_reload) {
                mBinding.ivShow.getDrawable().setLevel(0);
                mBinding.rvDetailHistory.setVisibility(View.GONE);
                mBinding.ivReload.setVisibility(View.GONE);
                mBinding.ivTimeline.setVisibility(View.GONE);
            }
        }
    }
}
