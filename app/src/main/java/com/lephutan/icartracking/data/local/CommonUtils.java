package com.lephutan.icartracking.data.local;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.lephutan.icartracking.App;

public class CommonUtils {
    private static final String FILE_PREF = "file_pref";
    private static CommonUtils instance;

    public CommonUtils() {
        //for singleton
    }

    public static CommonUtils getInstance() {
        if (instance == null) {
            instance = new CommonUtils();
        }
        return instance;
    }

    public void savePref(String key, String value) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        pref.edit().putString(key, value).apply();
    }

    public void saveBoolean(String key, boolean value) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        pref.edit().putBoolean(key, value).apply();
    }

    @SuppressLint("CommitPrefEdits")
    public void saveInt(String key, int value) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        pref.edit().putInt(key, value).apply();
    }

    public void clearPref(String key) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        pref.edit().remove(key).apply();
    }

    public String getPref(String key) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        return pref.getString(key, null);
    }

    public Boolean getBoolean(String key) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        return pref.getBoolean(key, false);
    }

    public Integer getInt(String key) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        return pref.getInt(key, -1);
    }
}
