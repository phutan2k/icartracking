package com.lephutan.icartracking.ui.main.info_user;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.CommonUtils;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.databinding.FrgM007InfoUserBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

public class M007InfoUserFrg extends BaseBindingFragment<FrgM007InfoUserBinding, M007InfoUserVM> {
    private String phone;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbar.tvTitle.setText(R.string.txt_user_information);

        mBinding.includeActionbar.ivBack.setOnClickListener(this);
        mBinding.ivAddAvatar.setOnClickListener(this);
        mBinding.tvButtonChangePass.setOnClickListener(this);
        mBinding.tvButtonUpdateInfo.setOnClickListener(this);

        initData();
        setupPhoneNumber();
        initInfo();
        observeData();
    }

    private void observeData() {
        mViewModel.accLogin.observe(getViewLifecycleOwner(), new Observer<AccountLogin>() {
            @Override
            public void onChanged(AccountLogin accountLogin) {
                String name = accountLogin.getData().getUsername();

                if (name == null) {
                    mBinding.tvUserName.setText(R.string.txt_chua_cap_nhat_ten);
                } else {
                    mBinding.tvUserName.setText(name);
                }
            }
        });
    }

    private void setupPhoneNumber() {
        phone = App.getInstance().getStorage().getPhoneNumber().getValue();

        if(phone == null){
            return;
        }
        mBinding.tvPhoneNumber.setText(phone);
    }

    private void initInfo() {
        String pass = App.getInstance().getStorage().getPass().getValue();

        if(pass == null){
            return;
        }
        mViewModel.loginAccount(phone, pass);
    }

    private void initData() {
        Integer stateImg = CommonUtils.getInstance().getInt(Constant.KEY_IMG);

        if (stateImg == null) {
            return;
        }

        if (stateImg != -1) {
            mBinding.ivAvatar.setImageResource(stateImg);
            return;
        }

        //
        Integer img = App.getInstance().getStorage().getImg().getValue();

        if (img == null) {
            mBinding.ivAvatar.setImageResource(R.drawable.ic_empty_avatar);
        } else {
            mBinding.ivAvatar.setImageResource(img);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void clickView(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBacked();
                break;
            case R.id.iv_add_avatar:
                showFrg(M007InfoUserFrgDirections.actionM007InfoUserFrgToM008ChoosePhotoFrg());
                break;
            case R.id.tv_button_change_pass:
                changePass();
                break;
            case R.id.tv_button_update_info:
                showFrg(M007InfoUserFrgDirections.actionM007InfoUserFrgToM013ChangeUsernameFrg());
                break;
        }
    }

    private void changePass() {
        showFrg(M007InfoUserFrgDirections.actionM007InfoUserFrgToM009ChangePassFrg());
    }

    @Override
    protected Class<M007InfoUserVM> getViewModel() {
        return M007InfoUserVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m007_info_user;
    }
}
