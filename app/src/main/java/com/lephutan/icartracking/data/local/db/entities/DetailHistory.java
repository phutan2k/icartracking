package com.lephutan.icartracking.data.local.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = "detail_history",
        primaryKeys = "id",
        foreignKeys =
                {
                        @ForeignKey(entity = History.class,
                                parentColumns = "date_id",
                                childColumns = "date_id")
                }
)
public class DetailHistory {
    @ColumnInfo(name = "id")
    public int id;
    @ColumnInfo(name = "date_id")
    public int dateId;
    @NonNull
    @ColumnInfo(name = "active_status")
    public String activeStatus;
    @NonNull
    @ColumnInfo(name = "last_address")
    public String lastAddress;
    @NonNull
    @ColumnInfo(name = "last_speed")
    public String lastSpeed;
    @NonNull
    @ColumnInfo(name = "last_lat")
    public String lastLat;
    @NonNull
    @ColumnInfo(name = "last_lng")
    public String lastLng;
    @NonNull
    @ColumnInfo(name = "last_status")
    public String lastStatus;
    @NonNull
    @ColumnInfo(name = "time_history")
    public String timeHistory;

    public DetailHistory(int id, int dateId,
                         @NonNull String activeStatus,
                         @NonNull String lastAddress,
                         @NonNull String lastSpeed,
                         @NonNull String lastLat,
                         @NonNull String lastLng,
                         @NonNull String lastStatus,
                         @NonNull String timeHistory) {
        this.id = id;
        this.dateId = dateId;
        this.activeStatus = activeStatus;
        this.lastAddress = lastAddress;
        this.lastSpeed = lastSpeed;
        this.lastLat = lastLat;
        this.lastLng = lastLng;
        this.lastStatus = lastStatus;
        this.timeHistory = timeHistory;
    }
}
