package com.lephutan.icartracking.data.repository;

import com.lephutan.icartracking.R;

import java.util.Arrays;
import java.util.List;

public class PhotoRepository {
    public List<Integer> getImageList() {
        return Arrays.asList(
                R.drawable.ic_cute_1,
                R.drawable.ic_cute_2,
                R.drawable.ic_cute_3,
                R.drawable.ic_cute_4,
                R.drawable.ic_cute_5,
                R.drawable.ic_cute_6,
                R.drawable.ic_cute_7,
                R.drawable.ic_cute_8,
                R.drawable.ic_cute_9,
                R.drawable.ic_cute_10
        );
    }
}
