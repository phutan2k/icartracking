package com.lephutan.icartracking.data.local.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "history",
        primaryKeys = "date_id",
        foreignKeys =
                {
                        @ForeignKey(entity = ICar.class,
                                parentColumns = "car_id",
                                childColumns = "car_id")
                }
)
public class History {
    @ColumnInfo(name = "date_id")
    public int dateId;
    @ColumnInfo(name = "car_id")
    public int carId;
    @NonNull
    @ColumnInfo(name = "date")
    public String date;

    public History(int dateId, int carId, @NonNull String date) {
        this.dateId = dateId;
        this.carId = carId;
        this.date = date;
    }
}
