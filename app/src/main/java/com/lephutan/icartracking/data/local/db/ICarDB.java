package com.lephutan.icartracking.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.data.local.db.entities.ICar;

@Database(version = Constant.VERSION_DB, entities = {ICar.class, History.class, DetailHistory.class})
public abstract class ICarDB extends RoomDatabase {
    public abstract ICarDao getICarDao();
}
