package com.lephutan.icartracking.ui.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.interfaces.OnButtonBackCallBack;
import com.lephutan.icartracking.interfaces.OnShowFrgCallBack;
import com.lephutan.icartracking.ui.main.MainViewModel;

public abstract class BaseBindingFragment<T extends ViewDataBinding, V extends BaseViewModel>
        extends BaseFragment implements View.OnClickListener, OnShowFrgCallBack, OnButtonBackCallBack {
    protected T mBinding;
    protected V mViewModel;
    protected MainViewModel mMainViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(getViewModel());
        mMainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        onCreatedView(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        clickView(v);
    }

    protected void clickView(View v) {
        // do something
    }

    protected final void notify(String sms) {
        Toast.makeText(requireContext(), sms, Toast.LENGTH_SHORT).show();
    }

    protected final void notify(int sms) {
        Toast.makeText(requireContext(), sms, Toast.LENGTH_SHORT).show();
    }

    protected final void animationText(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.abc_fade_in));
    }

    protected abstract void onCreatedView(View view, Bundle savedInstanceState);

    protected abstract Class<V> getViewModel();

    protected abstract int getLayoutId();

    @Override
    public void showFrg(NavDirections navDirections) {
        NavController navController = Navigation.findNavController(mBinding.getRoot());
        navController.navigate(navDirections);
    }

    @Override
    public void onBacked() {
        NavController navController = Navigation.findNavController(mBinding.getRoot());
        navController.navigateUp();
    }
}
