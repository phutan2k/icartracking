package com.lephutan.icartracking.ui.main.splash;

import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class M000SplashVM extends BaseViewModel {
    @Inject
    public M000SplashVM() {

    }
}
