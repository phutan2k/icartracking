package com.lephutan.icartracking.ui.main.car_list;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.data.repository.ICarLocalRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class M010CarListVM extends BaseViewModel {
    private final ICarLocalRepository iCarLocalRepository;
    MutableLiveData<List<ICar>> carList = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public M010CarListVM(ICarLocalRepository iCarLocalRepository) {
        this.iCarLocalRepository = iCarLocalRepository;
    }

    public void getCarList() {
        iCarLocalRepository.getCarList().subscribe(new SingleObserver<List<ICar>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull List<ICar> iCars) {
                carList.postValue(iCars);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.something_went_wrong);
            }
        });
    }
}
