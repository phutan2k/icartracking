package com.lephutan.icartracking.data.model.authen;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AccountLogin {
    @SerializedName("data")
    @Expose
    private UserData data;
    @SerializedName("message")
    @Expose
    private String message;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
