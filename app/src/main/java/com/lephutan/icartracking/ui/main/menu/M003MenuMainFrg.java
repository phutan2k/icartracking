package com.lephutan.icartracking.ui.main.menu;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.SupportMapFragment;
import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.CommonUtils;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.databinding.FrgM003MenuMainBinding;
import com.lephutan.icartracking.interfaces.OnActionCallBack;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;
import com.lephutan.icartracking.ui.dialog.DetailCarDialog;
import com.lephutan.icartracking.ui.google_map.MapManager;

import java.util.List;

public class M003MenuMainFrg extends BaseBindingFragment<FrgM003MenuMainBinding, M003MenuMainVM> {
    private String phone;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbar.ivMenu.setOnClickListener(this);
        mBinding.includeActionbar.ivLocation.setOnClickListener(this);
        mBinding.includeItemMenu.clInfoUser.setOnClickListener(this);
        mBinding.includeItemMenu.clMyLocation.setOnClickListener(this);
        mBinding.includeActionbar.ivListCar.setOnClickListener(this);
        mBinding.includeItemMenu.clListCars.setOnClickListener(this);
        mBinding.includeItemMenu.clLogoutApp.setOnClickListener(this);

        setupMap();
        initViews();
        initData();
        setupPhoneNumber();
        initInfo();
        observeData();
    }

    private void setupPhoneNumber() {
        phone = App.getInstance().getStorage().getPhoneNumber().getValue();
        if (phone == null) {
            return;
        }
        mBinding.includeItemMenu.tvPhone.setText(phone);
    }

    private void observeData() {
        notifyException();

        mViewModel.accLogin.observe(getViewLifecycleOwner(), new Observer<AccountLogin>() {
            @Override
            public void onChanged(AccountLogin accountLogin) {
                String name = accountLogin.getData().getUsername();

                if (name == null) {
                    mBinding.includeItemMenu.tvName.setText(R.string.txt_chua_cap_nhat_ten);
                } else {
                    mBinding.includeItemMenu.tvName.setText(name);
                }
            }
        });

        mViewModel.carList.observe(getViewLifecycleOwner(), new Observer<List<ICar>>() {
            @Override
            public void onChanged(List<ICar> iCars) {
                checkCarListWithPhoneNumber(iCars);
            }
        });
    }

    private void checkCarListWithPhoneNumber(List<ICar> iCars) {
        App.getInstance().getStorage().getPhoneNumber().observe(getViewLifecycleOwner(), s -> {
            if (s.equals(iCars.get(0).phoneManager)) {
                MapManager.getInstance().showCarList(iCars);
            } else {
                M003MenuMainFrg.this.notify(R.string.txt_khong_co_xe_trong_ds_quan_ly);
            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M003MenuMainFrg.this::notify);
    }

    private void initInfo() {
        String pass = App.getInstance().getStorage().getPass().getValue();

        if (pass == null) {
            return;
        }
        mViewModel.loginAccount(phone, pass);
    }

    private void initData() {
        Integer stateImg = CommonUtils.getInstance().getInt(Constant.KEY_IMG);

        if (stateImg == null) {
            return;
        }

        if (stateImg != -1) {
            mBinding.includeItemMenu.ivAvatar.setImageResource(stateImg);
            mBinding.includeItemMenu.ivInfoUser.setImageResource(stateImg);
            return;
        }

        //
        Integer img = App.getInstance().getStorage().getImg().getValue();

        if (img == null) {
            mBinding.includeItemMenu.ivAvatar.setImageResource(R.drawable.ic_empty_avatar);
            mBinding.includeItemMenu.ivInfoUser.setImageResource(R.drawable.ic_empty_avatar);
        } else {
            mBinding.includeItemMenu.ivAvatar.setImageResource(img);
            mBinding.includeItemMenu.ivInfoUser.setImageResource(img);
        }
    }

    private void setupMap() {
        SupportMapFragment mapFrg = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_frg);
        assert mapFrg != null;
        mapFrg.getMapAsync(googleMap -> MapManager.getInstance().initMap(googleMap));

        MapManager.getInstance().setCallBack(new OnActionCallBack() {
            @Override
            public void callBack(Object data, String key) {
                showCarDetail(data);
            }
        });
    }

    private void showCarDetail(Object data) {
        ICar car = (ICar) data;

        DetailCarDialog dialog = new DetailCarDialog(car);
        dialog.show(requireActivity().getSupportFragmentManager(), null);

        dialog.setCallBack(new OnActionCallBack() {
            @Override
            public void callBack(Object data, String key) {
                ICar iCar = (ICar) data;
                App.getInstance().getStorage().setCar(iCar);

                if (key.equals(Constant.KEY_SHOW_M011_MONITOR_FRG)) {
                    showFrg(M003MenuMainFrgDirections.actionM003MenuMainFrgToM011MonitorFrg());
                    dialog.dismiss();
                } else if (key.equals(Constant.KEY_SHOW_M004_TIME_LINE_FRG)) {
                    showFrg(M003MenuMainFrgDirections.actionM003MenuMainFrgToM004TimeLineFrg());
                    dialog.dismiss();
                }
            }
        });
    }

    private void initViews() {
        mBinding.drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                mBinding.includeActionbar.ivMenu.setImageLevel(Constant.LEVEL_OPEN);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                mBinding.includeActionbar.ivMenu.setImageLevel(Constant.LEVEL_CLOSE);
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void clickView(View v) {
        switch (v.getId()) {
            case R.id.iv_menu:
                openDrawer();
                break;
            case R.id.iv_location:
            case R.id.cl_my_location:
                MapManager.getInstance().updateMyLocation();
                break;
            case R.id.cl_info_user:
                showFrg(M003MenuMainFrgDirections.actionM003MenuMainFrgToM007InfoUserFrg());
                break;
            case R.id.iv_list_car:
                mViewModel.getCarList();
                break;
            case R.id.cl_list_cars:
                showFrg(M003MenuMainFrgDirections.actionM003MenuMainFrgToM010CarListFrg());
                break;
            case R.id.cl_logout_app:
                handleLogout();
                break;
        }
    }

    private void handleLogout() {
        CommonUtils.getInstance().clearPref(Constant.KEY_IMG);
        CommonUtils.getInstance().clearPref(Constant.KEY_ID);
        showFrg(M003MenuMainFrgDirections.actionM003MenuMainFrgToM002LoginFrg());
    }

    private void openDrawer() {
        if (mBinding.includeActionbar.ivMenu.getDrawable().getLevel() == Constant.LEVEL_OPEN) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            mBinding.drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    protected Class<M003MenuMainVM> getViewModel() {
        return M003MenuMainVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m003_menu_main;
    }
}
