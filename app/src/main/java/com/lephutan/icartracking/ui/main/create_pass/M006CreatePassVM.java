package com.lephutan.icartracking.ui.main.create_pass;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.repository.ICarRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@HiltViewModel
public class M006CreatePassVM extends BaseViewModel {
    private final ICarRepository iCarRepository;
    MutableLiveData<AccountLogin> forgotPass = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public M006CreatePassVM(ICarRepository iCarRepository) {
        this.iCarRepository = iCarRepository;
    }

    public void forgotPass(String phone, String newPass) {
        iCarRepository.forgotPassword(phone, newPass).subscribe(new SingleObserver<AccountLogin>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull AccountLogin accountLogin) {
                forgotPass.postValue(accountLogin);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.something_went_wrong);
            }
        });
    }

    public String changeNumberFormat(String phone) {
        String basket = phone.substring(1);
        return "+84" + basket;
    }

    public int validate(String password, String reEnterPassword) {
        if (password.isEmpty() || reEnterPassword.isEmpty()) {
            return Constant.PHONE_OR_PASSWORD_IS_EMPTY;
        } else if (!checkPassword(password, reEnterPassword)) {
            return Constant.INVALID_PASSWORD;
        } else if (!duplicateCheck(password, reEnterPassword)) {
            return Constant.PASSWORD_INCORRECT;
        }

        return Constant.LOGIN_SUCCESS;
    }

    private boolean duplicateCheck(String password, String reEnterPassword) {
        return password.equals(reEnterPassword);
    }

    private boolean checkPassword(String password, String reEnterPassword) {
        return password.length() >= 6 && reEnterPassword.length() >= 6;
    }
}
