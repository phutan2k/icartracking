package com.lephutan.icartracking.ui.base;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.fragment.NavHostFragment;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.interfaces.OnHomeCallBack;

public abstract class BaseBindingActivity<T extends ViewDataBinding, V extends BaseViewModel>
        extends BaseActivity implements View.OnClickListener, OnHomeCallBack {
    protected T mBinding;
    protected V mViewModel;
    protected NavHostFragment mNavHostFragment;
    protected NavController mNavController;
    protected NavGraph mGraph;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, getLayoutId());
        mViewModel = new ViewModelProvider(this).get(getViewModel());

        mNavHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment_container);

        if (mNavHostFragment != null) {
            mNavController = mNavHostFragment.getNavController();
        }

        setupView(savedInstanceState);
        setupData();
    }

    @Override
    public void onClick(View v) {
        clickView(v);
    }

    private void clickView(View v) {
        // do something
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        int level = mNavHostFragment.getChildFragmentManager().getBackStackEntryCount();

        if (level == 0) {
            super.onBackPressed();
        }
    }

    protected final void notify(String sms) {
        Toast.makeText(this, sms, Toast.LENGTH_SHORT).show();
    }

    protected final void notify(int sms) {
        Toast.makeText(this, sms, Toast.LENGTH_SHORT).show();
    }

    protected abstract void setupData();

    protected abstract void setupView(Bundle savedInstanceState);

    protected abstract Class<V> getViewModel();

    protected abstract int getLayoutId();

    @Override
    public void changeStartDestination(int idScreen) {
        if (mGraph == null) {
            mGraph = mNavController.getNavInflater().inflate(R.navigation.nav_main);
        }
        mGraph.setStartDestination(idScreen);
        mNavController.setGraph(mGraph);
    }
}
