package com.lephutan.icartracking.ui.main.splash;

import static android.view.View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
import static android.view.View.VISIBLE;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.databinding.FrgM000SplashBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

public class M000SplashFrg extends BaseBindingFragment<FrgM000SplashBinding, M000SplashVM> {
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected Class<M000SplashVM> getViewModel() {
        return M000SplashVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m000_splash;
    }

}
