package com.lephutan.icartracking.ui.main.time_line;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.DetailHistory;
import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.databinding.FrgM004TimeLineBinding;
import com.lephutan.icartracking.interfaces.OnActionCallBack;
import com.lephutan.icartracking.ui.adapter.TimeLineAdapter;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

import java.util.List;

public class M004TimeLineFrg extends BaseBindingFragment<FrgM004TimeLineBinding, M004TimeLineVM> {
    private ICar car;
    private TimeLineAdapter adapter;

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        car = App.getInstance().getStorage().getCar().getValue();

        if (car == null) {
            return;
        }

        mBinding.tvTitle.setText(R.string.journey_history);
        mBinding.ivBack.setOnClickListener(this);
        mBinding.ivSearch.setOnClickListener(this);

        initData();
        observeData();
        showListHistoryInDay();
    }

    private void observeData() {
        notifyException();

        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        mBinding.rvHistory.setLayoutManager(layoutManager);

        mViewModel.historyList.observe(getViewLifecycleOwner(), new Observer<List<History>>() {
            @Override
            public void onChanged(List<History> histories) {
                if (adapter == null) {
                    adapter = new TimeLineAdapter(requireContext());
                    adapter.setHistoryList(histories);
                    mBinding.rvHistory.setAdapter(adapter);

                    detailHistoryList();
                }
            }
        });
    }

    private void detailHistoryList() {
        adapter.setCallBack(new OnActionCallBack() {
            @Override
            public void callBack(Object data, String key) {
                History history = (History) data;
                App.getInstance().getStorage().setHistory(history);

                if (key.equals(Constant.KEY_GET_DETAIL_HISTORY)) {
                    mViewModel.getDetailHistoryList(history.dateId);
                } else if (key.equals(Constant.KEY_SHOW_M012_MONITOR_BY_DAY_FRG)) {
                    showFrg(M004TimeLineFrgDirections.actionM004TimeLineFrgToM012MonitorByDayFrg());
                }
            }
        });
    }

    private void showListHistoryInDay() {
        notifyException();

        mViewModel.detailHistoryList.observe(getViewLifecycleOwner(), new Observer<List<DetailHistory>>() {
            @SuppressLint("LogNotTimber")
            @Override
            public void onChanged(List<DetailHistory> data) {
                if (data == null) return;
                Log.i("DetailHistoryIsTan", data.get(0).lastAddress);

                adapter.setDetailHistoryList(data);

            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M004TimeLineFrg.this::notify);
    }

    private void initData() {
        mViewModel.getHistoryList(car.carId);
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.iv_back) {
            onBacked();
        }
    }

    @Override
    protected Class<M004TimeLineVM> getViewModel() {
        return M004TimeLineVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m004_time_line;
    }
}
