package com.lephutan.icartracking.ui.main.forgot_pass;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.repository.ICarRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class M005ForgotPassVM extends BaseViewModel {
    private final ICarRepository iCarRepository;
    MutableLiveData<Integer> sms = new MutableLiveData<>();
    MutableLiveData<AccountLogin> checkPhone = new MutableLiveData<>();

    @Inject
    public M005ForgotPassVM(ICarRepository iCarRepository) {
        this.iCarRepository = iCarRepository;
    }

    public void checkPhoneNumber(String phone) {
        iCarRepository.checkPhone(phone).subscribe(new SingleObserver<AccountLogin>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull AccountLogin accountLogin) {
                checkPhone.postValue(accountLogin);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.something_went_wrong);
            }
        });
    }

    public int validate(String phone) {
        if (phone.isEmpty()) {
            return Constant.PHONE_OR_PASSWORD_IS_EMPTY;
        } else if (!checkPhone(phone)) {
            return Constant.INVALID_PHONE_NUMBER;
        }

        return Constant.LOGIN_SUCCESS;
    }

    private boolean checkPhone(String phone) {
        String basket = "";

        for (int i = 0; i < phone.length(); i++) {
            char characters = phone.charAt(i);

            if (characters >= '0' && characters <= '9') {
                basket += characters;
            } else {
                return false;
            }
        }
        long number = Long.parseLong(basket);

        return number > 0;
    }
}
