package com.lephutan.icartracking.interfaces;

import androidx.navigation.NavDirections;

public interface OnShowFrgCallBack {
    void showFrg(NavDirections navDirections);
}
