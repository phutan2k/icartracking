package com.lephutan.icartracking.ui.main.register;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.repository.ICarRepository;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class M001RegisterVM extends BaseViewModel {
    private final ICarRepository iCarRepository;
    MutableLiveData<AccountLogin> accRegister = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public M001RegisterVM(ICarRepository iCarRepository) {
        this.iCarRepository = iCarRepository;
    }

    public void registerAccount(String phone, String password) {
        iCarRepository.registerAccount(phone, password)
                .subscribe(new SingleObserver<AccountLogin>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull AccountLogin accountLogin) {
                        accRegister.postValue(accountLogin);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.account_registration_failed);
                    }
                });
    }

    public int validate(String phone, String password, String reEnterPassword) {
        if (phone.isEmpty() || password.isEmpty() || reEnterPassword.isEmpty()) {
            return Constant.PHONE_OR_PASSWORD_IS_EMPTY;
        } else if (!checkPhone(phone)) {
            return Constant.INVALID_PHONE_NUMBER;
        } else if (!checkPassword(password, reEnterPassword)) {
            return Constant.INVALID_PASSWORD;
        } else if (!duplicateCheck(password, reEnterPassword)) {
            return Constant.PASSWORD_INCORRECT;
        }

        return Constant.LOGIN_SUCCESS;
    }

    private boolean duplicateCheck(String password, String reEnterPassword) {
        return password.equals(reEnterPassword);
    }

    private boolean checkPassword(String password, String reEnterPassword) {
        return password.length() >= 6 && reEnterPassword.length() >= 6;
    }

    private boolean checkPhone(String phone) {
        String basket = "";

        for (int i = 0; i < phone.length(); i++) {
            char characters = phone.charAt(i);

            if (characters >= '0' && characters <= '9') {
                basket += characters;
            } else {
                return false;
            }
        }
        long number = Long.parseLong(basket);

        return number > 0;
    }


    public String changeNumberFormat(String phone) {
        String basket = phone.substring(1);
        return "+84" + basket;
    }
}
