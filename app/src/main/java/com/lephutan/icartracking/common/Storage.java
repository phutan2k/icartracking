package com.lephutan.icartracking.common;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.data.local.db.entities.History;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.data.model.authen.AccountLogin;

public final class Storage {
    private final MutableLiveData<String> phoneNumber = new MutableLiveData<>();
    private final MutableLiveData<Integer> img = new MutableLiveData<>();
    private final MutableLiveData<Integer> pos = new MutableLiveData<>();
    private final MutableLiveData<String> name = new MutableLiveData<>();
    private final MutableLiveData<String> token = new MutableLiveData<>();
    private final MutableLiveData<ICar> car = new MutableLiveData<>();
    private final MutableLiveData<History> history = new MutableLiveData<>();
    private final MutableLiveData<String> pass = new MutableLiveData<>();
//    private final MutableLiveData<AccountLogin> data = new MutableLiveData<>();

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phone) {
        phoneNumber.postValue(phone);
    }

    public MutableLiveData<Integer> getImg() {
        return img;
    }

    public void setImg(int image) {
        img.postValue(image);
    }

    public MutableLiveData<Integer> getPos() {
        return pos;
    }

    public void setPos(int position) {
        pos.postValue(position);
    }

    public MutableLiveData<String> getName() {
        return name;
    }

    public void setName(String n) {
        name.postValue(n);
    }

    public MutableLiveData<String> getToken() {
        return token;
    }

    public void setToken(String tk) {
        token.postValue(tk);
    }

    public MutableLiveData<ICar> getCar() {
        return car;
    }

    public void setCar(ICar car) {
        this.car.postValue(car);
    }

    public MutableLiveData<History> getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history.postValue(history);
    }

//    public MutableLiveData<AccountLogin> getData() {
//        return data;
//    }
//
//    public void setData(AccountLogin data) {
//        this.data.postValue(data);
//    }

    public MutableLiveData<String> getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass.postValue(pass);
    }
}
