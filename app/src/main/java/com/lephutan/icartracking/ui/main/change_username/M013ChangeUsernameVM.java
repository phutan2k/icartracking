package com.lephutan.icartracking.ui.main.change_username;

import androidx.lifecycle.MutableLiveData;

import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.UserData;
import com.lephutan.icartracking.data.remote.ICarApi;
import com.lephutan.icartracking.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@HiltViewModel
public class M013ChangeUsernameVM extends BaseViewModel {
    private final ICarApi iCarApi;
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public M013ChangeUsernameVM(ICarApi iCarApi) {
        this.iCarApi = iCarApi;
    }

    public void updateUsername(String token, String name) {
        iCarApi.updateProfile("Bearer " + token, name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserData>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull UserData userData) {
                        sms.postValue(R.string.txt_cap_nhat_ten_thanh_cong);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.something_went_wrong);
                    }
                });
    }

    public int checkUsername(String username) {
        if (username.equals("") || username.isEmpty()) {
            return Constant.USER_NAME_IS_EMPTY;
        }

        return Constant.LOGIN_SUCCESS;
    }
}
