package com.lephutan.icartracking.ui.google_map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.interfaces.OnActionCallBack;
import com.lephutan.icartracking.ui.dialog.DetailCarDialog;
import com.lephutan.icartracking.ui.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class MapManager {
    public static final String TAG = MapManager.class.getName();
    private static MapManager instance;
    private GoogleMap mMap;

    private Location myLocation;
    private Marker mMarker;
    private List<Marker> carMarkerList;
    private OnActionCallBack callBack;

    public MapManager() {
        // for singleton
    }

    public static MapManager getInstance() {
        if (instance == null) {
            instance = new MapManager();
        }
        return instance;
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    public Location getMyLocation() {
        return myLocation;
    }

    @SuppressLint("VisibleForTests")
    public void initMap(GoogleMap map) {
        mMap = map;
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Xét click ở marker
//        mMap.setInfoWindowAdapter(initWindow());
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {
                if (marker.getTag() == null) return false;

                ICar car = (ICar) marker.getTag();
                showCarInfo(car);

                return true;
            }
        });

        if (ActivityCompat.checkSelfPermission(App.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(App.getInstance(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            Toast.makeText
                    (App.getInstance(), R.string.ban_chua_cap_quyen_vi_tri, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        LocationRequest request = new LocationRequest();
        request.setInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        FusedLocationProviderClient mClient = new FusedLocationProviderClient(App.getInstance());
        mClient.requestLocationUpdates(request, new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                // khi thay đổi vị trí nó sẽ cập nhật vào đây
                updateMyLocation(locationResult);
            }
        }, Looper.getMainLooper());
    }

    @SuppressLint("LogNotTimber")
    private void updateMyLocation(LocationResult rs) {
        Location location = rs.getLastLocation();
        Log.i(TAG, "updateMyLocation..." + location.toString());

        if (myLocation == null) {
            myLocation = location;
            LatLng pos = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));

            // hiển thị marker hình giọt nước
            MarkerOptions op = new MarkerOptions();

            op.title("My location");
            op.position(pos);
            op.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            mMarker = mMap.addMarker(op);
        } else {
            myLocation = location;
        }
    }

    public void updateMyLocation() {
        if (mMap == null || myLocation == null) return;
        LatLng pos = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        mMarker.setPosition(pos);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));
    }

    @SuppressLint("LogNotTimber")
    public void showCarList(List<ICar> iCars) {
        carMarkerList = new ArrayList<>();
        Log.i("TanLe", iCars.size() + "");

        for (int i = 0; i < carMarkerList.size(); i++) {
            carMarkerList.get(i).remove();
        }
        carMarkerList.clear();

        for (int i = 0; i < iCars.size(); i++) {
            showCarOnMap(iCars.get(i), i);
        }
    }

    private void showCarOnMap(ICar car, int index) {
        double lat = Double.parseDouble(car.lastLat);
        double lng = Double.parseDouble(car.lastLng);
        LatLng pos = new LatLng(lat, lng);

        MarkerOptions op = new MarkerOptions();
        op.title(car.carNumber);
        op.position(pos);
        op.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_location));

        Marker marker = mMap.addMarker(op);
        carMarkerList.add(marker);

        assert marker != null;
        marker.setTag(car);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 12));
//        if (index == 0) {
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 12));
//        }
    }

/*    private GoogleMap.InfoWindowAdapter initWindow() {
        return new GoogleMap.InfoWindowAdapter() {
            @Nullable
            @Override
            public View getInfoContents(@NonNull Marker marker) {
                return initViewsAdapter(marker);
            }

            @Nullable
            @Override
            public View getInfoWindow(@NonNull Marker marker) {
                return initViewsAdapter(marker);
            }
        };
    }*/

/*    private View initViewsAdapter(Marker marker) {
        if (marker.getTag() == null) return null;

        ICar car = (ICar) marker.getTag();

        View view = LayoutInflater.from(App.getInstance())
                .inflate(R.layout.view_detail_car, null);

        TextView tvCarNumber = view.findViewById(R.id.tv_car_number);
        TextView tvCarName = view.findViewById(R.id.tv_car_name);
        TextView tvStatus = view.findViewById(R.id.tv_last_status);
        TextView tvSpeed = view.findViewById(R.id.tv_last_speed);
        TextView tvAddress = view.findViewById(R.id.tv_last_address);

        tvCarNumber.setText(car.carNumber);
        tvCarName.setText(car.carName);
        tvStatus.setText(car.lastStatus);
        tvSpeed.setText(car.lastSpeed);
        tvAddress.setText(car.lastAddress);

        return view;
    }*/

    private void showCarInfo(ICar car) {
        callBack.callBack(car, Constant.KEY_SHOW_DETAIL_CAR);
    }
}
