package com.lephutan.icartracking.data.remote;

import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.data.model.authen.UserData;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ICarApi {
    @FormUrlEncoded
    @POST("auth/login")
    Single<AccountLogin> loginAccount(@Field("phone") String phone,
                                      @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/register")
    Single<AccountLogin> registerAccount(@Field("phone") String phone,
                                         @Field("password") String password);

    @GET("auth/check")
    Single<AccountLogin> checkPhone(@Query("phone") String phone);

    @FormUrlEncoded
    @POST("auth/forgot_password")
    Single<AccountLogin> forgotPassword(@Field("phone") String phone,
                                        @Field("new_password") String newPassword);

    @FormUrlEncoded
    @PUT("users/update_profile")
    Single<UserData> updateProfile(@Header("Authorization") String authorization,
                                   @Field("username") String username);

    @FormUrlEncoded
    @PUT("users/change_password")
    Completable changePassword(@Header("Authorization") String authorization,
                               @Field("old_password") String odlPass,
                               @Field("new_password") String newPass);
}
