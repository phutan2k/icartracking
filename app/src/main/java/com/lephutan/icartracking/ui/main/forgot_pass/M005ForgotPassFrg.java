package com.lephutan.icartracking.ui.main.forgot_pass;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.databinding.FrgM005ForgotPassBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;

public class M005ForgotPassFrg extends BaseBindingFragment<FrgM005ForgotPassBinding, M005ForgotPassVM> {
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        mBinding.includeActionbarForgotPass.tvTitle.setText(R.string.txt_lay_lai_mat_khau);

        mBinding.tvButtonPasswordRetrieval.setOnClickListener(this);
        mBinding.includeActionbarForgotPass.ivBack.setOnClickListener(this);

        observeData();
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.tv_button_password_retrieval) {
            checkPhone();
        } else if (v.getId() == R.id.iv_back) {
            onBacked();
        }
    }

    private void checkPhone() {
        String phone = mBinding.edtPhone.getText().toString();

        int result = mViewModel.validate(phone);

        switch (result) {
            case Constant.PHONE_OR_PASSWORD_IS_EMPTY:
                notify(R.string.txt_phone_number_or_password_is_empty);
                break;
            case Constant.INVALID_PHONE_NUMBER:
                notify(R.string.txt_invalid_phone_number);
                break;
            case Constant.LOGIN_SUCCESS:
                mViewModel.checkPhoneNumber(phone);
                break;
        }
    }

    private void observeData() {
        notifyException();

        mViewModel.checkPhone.observe(getViewLifecycleOwner(), new Observer<AccountLogin>() {
            @Override
            public void onChanged(AccountLogin accountLogin) {
                if (accountLogin.getMessage().equals("Tài khoản đã tồn tại")) {
                    // Đưa dữ liệu phone từ màn m005 sang m006
                    App.getInstance().getStorage().setPhoneNumber(accountLogin.getData().getPhone());

                    // show màn m006
                    showFrg(M005ForgotPassFrgDirections.actionM005ForgotPassFrgToM006CreatePassFrg());
                } else {
                    M005ForgotPassFrg.this.notify(R.string.phone_number_does_not_exist);
                }
            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M005ForgotPassFrg.this::notify);
    }

    @Override
    protected Class<M005ForgotPassVM> getViewModel() {
        return M005ForgotPassVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m005_forgot_pass;
    }
}
