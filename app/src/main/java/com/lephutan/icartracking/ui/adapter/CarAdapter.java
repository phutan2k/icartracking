package com.lephutan.icartracking.ui.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.local.db.entities.ICar;
import com.lephutan.icartracking.databinding.ItemCarDetailBinding;
import com.lephutan.icartracking.interfaces.OnActionCallBack;

import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarHolder> {
    private List<ICar> carList;
    private OnActionCallBack callBack;

    @SuppressLint("NotifyDataSetChanged")
    public void setCarList(List<ICar> carList) {
        this.carList = carList;
        notifyDataSetChanged();
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public CarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCarDetailBinding mBinding = ItemCarDetailBinding.inflate
                (
                        LayoutInflater.from(parent.getContext()), parent, false
                );
        return new CarHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CarHolder holder, int position) {
        holder.bind(carList.get(position));
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    public class CarHolder extends RecyclerView.ViewHolder {
        ItemCarDetailBinding mBinding;

        public CarHolder(@NonNull ItemCarDetailBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(ICar car) {
            mBinding.tvCarName.setText(car.carName);
            mBinding.tvCarBrand.setText(car.carBrand);
            mBinding.tvCarNumber.setText(car.carNumber);

            mBinding.tvButtonMonitor.setOnClickListener(v ->
                    callBack.callBack(car, Constant.KEY_SHOW_M011_MONITOR_FRG));

            mBinding.tvButtonHistory.setOnClickListener(v ->
                    callBack.callBack(car, Constant.KEY_SHOW_M004_TIME_LINE_FRG));
        }
    }
}
