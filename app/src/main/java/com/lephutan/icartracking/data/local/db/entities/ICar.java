package com.lephutan.icartracking.data.local.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "icar", primaryKeys = "car_id")
public class ICar {
    @ColumnInfo(name = "car_id")
    public int carId;
    @NonNull
    @ColumnInfo(name = "car_name")
    public String carName;
    @NonNull
    @ColumnInfo(name = "phone_manager")
    public String phoneManager;
    @NonNull
    @ColumnInfo(name = "car_number")
    public String carNumber;
    @NonNull
    @ColumnInfo(name = "car_brand")
    public String carBrand;
    @NonNull
    @ColumnInfo(name = "active_status")
    public String activeStatus;
    @NonNull
    @ColumnInfo(name = "last_address")
    public String lastAddress;
    @NonNull
    @ColumnInfo(name = "last_speed")
    public String lastSpeed;
    @NonNull
    @ColumnInfo(name = "last_lat")
    public String lastLat;
    @NonNull
    @ColumnInfo(name = "last_lng")
    public String lastLng;
    @NonNull
    @ColumnInfo(name = "last_status")
    public String lastStatus;

    public ICar(int carId,
                @NonNull String carName,
                @NonNull String phoneManager,
                @NonNull String carNumber,
                @NonNull String carBrand,
                @NonNull String activeStatus,
                @NonNull String lastAddress,
                @NonNull String lastSpeed,
                @NonNull String lastLat,
                @NonNull String lastLng,
                @NonNull String lastStatus) {
        this.carId = carId;
        this.carName = carName;
        this.phoneManager = phoneManager;
        this.carNumber = carNumber;
        this.carBrand = carBrand;
        this.activeStatus = activeStatus;
        this.lastAddress = lastAddress;
        this.lastSpeed = lastSpeed;
        this.lastLat = lastLat;
        this.lastLng = lastLng;
        this.lastStatus = lastStatus;
    }
}
