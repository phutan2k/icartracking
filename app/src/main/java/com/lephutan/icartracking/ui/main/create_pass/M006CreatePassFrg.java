package com.lephutan.icartracking.ui.main.create_pass;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;

import com.lephutan.icartracking.App;
import com.lephutan.icartracking.R;
import com.lephutan.icartracking.common.Constant;
import com.lephutan.icartracking.data.model.authen.AccountLogin;
import com.lephutan.icartracking.databinding.FrgM006CreatePassBinding;
import com.lephutan.icartracking.ui.base.BaseBindingFragment;
import com.lephutan.icartracking.ui.dialog.CallAuthenDialog;

public class M006CreatePassFrg extends BaseBindingFragment<FrgM006CreatePassBinding, M006CreatePassVM> {

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        App.getInstance().getStorage().getPhoneNumber().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                mBinding.tvPhone.setText(s);
            }
        });

        mBinding.tvButtonCreatePass.setOnClickListener(this);

        observeData();
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.tv_button_create_pass) {
            checkPass();
        }
    }

    private void checkPass() {
        String phone = mBinding.tvPhone.getText().toString();

        String newPhoneNumber = mViewModel.changeNumberFormat(phone);
        String password = mBinding.edtPass.getText().toString();
        String reEnterPassword = mBinding.edtReEnterPass.getText().toString();

        int result = mViewModel.validate(password, reEnterPassword);

        switch (result) {
            case Constant.PHONE_OR_PASSWORD_IS_EMPTY:
                notify(R.string.txt_phone_number_or_password_is_empty);
                break;
            case Constant.INVALID_PASSWORD:
                notify(R.string.txt_invalid_password);
                break;
            case Constant.PASSWORD_INCORRECT:
                notify(R.string.txt_password_incorrect);
                break;
            case Constant.LOGIN_SUCCESS:
                // Call dịch vụ fire base để lấy số điện thoại
                showDialog(newPhoneNumber);

                mViewModel.forgotPass(phone, password);
                break;
        }
    }

    private void observeData() {
        notifyException();

        mViewModel.forgotPass.observe(getViewLifecycleOwner(), new Observer<AccountLogin>() {
            @Override
            public void onChanged(AccountLogin accountLogin) {
                if (accountLogin.getMessage().equals("Cập nhật mật khẩu mới thành công")) {
                    showFrg(M006CreatePassFrgDirections.actionM006CreatePassFrgToM002LoginFrg());
                } else {
                    M006CreatePassFrg.this.notify(R.string.new_password_creation_failed);
                }
            }
        });
    }

    private void notifyException() {
        mViewModel.sms.observe(getViewLifecycleOwner(), M006CreatePassFrg.this::notify);
    }

    private void showDialog(String phone) {
        CallAuthenDialog dialog = new CallAuthenDialog(phone);
        dialog.show(requireActivity().getSupportFragmentManager(), null);
    }

    @Override
    protected Class<M006CreatePassVM> getViewModel() {
        return M006CreatePassVM.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m006_create_pass;
    }
}
